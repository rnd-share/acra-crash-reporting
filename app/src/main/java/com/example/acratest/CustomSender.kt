package com.example.acratest

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.auto.service.AutoService
import org.acra.ReportField
import org.acra.attachment.AttachmentUriProvider
import org.acra.attachment.DefaultAttachmentProvider
import org.acra.config.BaseHttpConfigurationBuilder
import org.acra.config.ConfigUtils
import org.acra.config.CoreConfiguration
import org.acra.config.HttpSenderConfiguration
import org.acra.data.CrashReportData
import org.acra.data.StringFormat
import org.acra.sender.HttpSender
import org.acra.sender.ReportSender
import org.acra.sender.ReportSenderException
import org.acra.sender.ReportSenderFactory
import org.acra.util.InstanceCreator
import org.json.JSONObject
import java.net.URL
import kotlin.contracts.contract

class CustomSender(configuration: CoreConfiguration, method: Method = Method.POST, type: StringFormat? = null)
    : HttpSender(configuration, method, type) {

    override fun send(context: Context, report: CrashReportData) {
        try {
            report.put(
                ReportField.PHONE_MODEL,
                report.getString(ReportField.PHONE_MODEL) + getAdditionalInfo()
            )
            //report.put(BuildConfig.IP_ADDRESS, BuildConfig.IP_ADDRESS)
            Log.e("YourOwnSender", report.toJSON())
            super.send(context, report)
        } catch (e: Exception) {
            throw ReportSenderException(
                "Error while sending JSON report via Http POST",
                e
            )
        }

    }

    private fun getAdditionalInfo(): String {
        return " ABC-001"
    }
}

@AutoService(ReportSenderFactory::class)
class SenderFactory: ReportSenderFactory {
    override fun create(context: Context, config: CoreConfiguration): ReportSender {
        return CustomSender(config, HttpSender.Method.POST, StringFormat.JSON)
    }

}